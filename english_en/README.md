# How to help us translate and do corrections

1. Find a file that hasn't been translated yet.
![example](https://i.imgur.com/R9pMlch.png)
2. Click edit in the right corner.
3. If it asks you to fork, click "fork"
4. Translate the text or do the changes.
5. Once done click "commit changes".
![example](https://i.imgur.com/i1WN6O3.png)
6. On the next page you can add a comment if you want.
7. Scroll to the bottom and click "Submit merge request".
![example](https://i.imgur.com/4Cqk1HU.png)
8. Now you are done, wait until we hopefully aprove your changes.